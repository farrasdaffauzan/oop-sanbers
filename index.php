<?php

// require ('animal.php');
// require('frog.php');
require('ape.php');

$sheep = new animal("shaun");

echo "Nama Hewan ; " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " .  $sheep->cold_blooded . "<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new frog("buduk");


echo " <br> Nama Hewan ; " . $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $kodok->legs . "<br>"; // 4
echo "Cold Blooded : " .  $kodok->cold_blooded . "<br>"; // "no"
$kodok->jump(); // "hop hop"

$sungokong = new ape("kera sakti");

echo " <br> <br> Nama Hewan ; " . $sungokong->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sungokong->legs . "<br>"; // 4
echo "Cold Blooded : " .  $sungokong->cold_blooded . "<br>"; // "no"
$sungokong->yell() // "Auooo"



?>